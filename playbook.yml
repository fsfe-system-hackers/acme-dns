---
- name: "Deploy ACME DNS"
  hosts: acme_dns

  tasks:
    - name: "Install base packages"
      apt:
        name:
        - htop
        - bash-completion
        - vim
        - wget
        - curl
        - golang
        - ufw

    - name: Copy acme-dns upstream files
      copy:
        src: "{{ item }}"
        dest: /root/acme-dns/
      with_fileglob:
        - "acme-dns/*.go"
        - "acme-dns/go*"

    - name: Compile acme-dns
      command: chdir=/root/acme-dns go build
      environment:
        GOPATH: /tmp/acme-dns

    - name: Copy acme-dns binary
      copy:
        src: /root/acme-dns/acme-dns
        dest: /usr/local/bin/
        remote_src: true
        mode: 0755

    - name: Deploy acme-dns configuration
      copy:
        src: config.cfg
        dest: /etc/acme-dns/

    - name: Add minimal acme-dns user
      user:
        name: acme-dns
        password: '*'
        home: /var/lib/acme-dns
        shell: /usr/sbin/nologin

    - name: Create systemd service
      copy:
        src: acme-dns/acme-dns.service
        dest: /etc/systemd/system/

    - name: Enable and start acme-dns service
      systemd:
        name: acme-dns
        state: started
        enabled: yes
        daemon_reload: yes

    - name: Enable UFW
      ufw:
        state: enabled
        policy: allow

    - name: Disallow access to port 443
      ufw:
        port: "443"
        rule: deny

    - name: Allow access to port 443 for FSFE IPs
      ufw:
        port: "443"
        rule: allow
        src: "{{ item | replace('[','') | replace(']','') }}"
        # insert them above of the deny rule
        insert: 0
        insert_relative_to: last-ipv6
      loop: "{{ mynetworks }}"

- name: "Deploy ACME DNS Client"
  hosts: baseline
  tasks:
    - name: Run acme-dns-client role
      include_role:
        name: acme-dns-client
